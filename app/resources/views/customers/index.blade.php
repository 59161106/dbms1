<table class="table" border="1">
<thead>
    <th>#id</th>
    <th>CustNo</th>
    <th>CustFirstName</th>
    <th>CustLastName</th>
    <th>CustStreet</th>
    <th>CustCity</th>
    <th>CustState</th>
    <th>CustZip</th>
    <th>CustBal</th>
</thead>
<tbody>
@foreach($customers as $customer)
<tr>
    <td>{{ $customer->id }}</td>
    <td>{{ $customer->CustNo }}</td>
    <td>{{ $customer->CustFirstName }}</td>
    <td>{{ $customer->CustLastName }}</td>
    <td>{{ $customer->CustStreet }}</td>
    <td>{{ $customer->CustCity }}</td>
    <td>{{ $customer->CustState }}</td>
    <td>{{ $customer->CustZip }}</td>
    <td>{{ $customer->CustBal }}</td>
</tr>
@endforeach
</tbody>
</table>