<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableEmployees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Employees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('EmpNo');
            $table->string('EmpFirstName');
            $table->string('EmpLastName');
            $table->string('EmpPhone');
            $table->string('EmpEMail');
            $table->string('SupEmpNo');
            $table->double('EmpCommRate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Employees');
    }
}
