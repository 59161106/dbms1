<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableCustomers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() //method สร้าง
    {
        Schema::create('Customers', function (Blueprint $table) {
            $table->bigIncrements('id'); //don't del
            $table->string('CustNo');
            $table->string('CustFirstName');
            $table->string('CustLastName');
            $table->string('CustStreet');
            $table->string('CustCity');
            $table->string('CustState');
            $table->string('CustZip');
            $table->double('CustBal');
            $table->timestamps(); //don't del
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() //method ลบ
    {
        Schema::dropIfExists('Customers');
    }
}
