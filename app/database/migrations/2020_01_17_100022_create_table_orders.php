<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('OrdNo');
            $table->date('OrdDate');
            $table->string('CustNo');
            $table->string('EmpNo');
            $table->string('OrdName');
            $table->string('OrdStreet');
            $table->string('OrdCity');
            $table->string('OrdState');
            $table->string('OrdZip');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Orders');
    }
}
